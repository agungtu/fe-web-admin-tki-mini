# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import FastAPI, Request, Depends
from fastapi.security import OAuth2PasswordRequestForm
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from starlette.middleware.sessions import SessionMiddleware
import requests
from starlette.responses import RedirectResponse

from route.crm.crm_partner import crm_partner
from route.crm.crm_client import crm_client
from route.crm.crm_validator import crm_validator

app = FastAPI(openapi_url="/web_crm/openapi.json",docs_url="/web_crm/swgr")
app.mount("/web_crm/static", StaticFiles(directory="static"), name="static")
app.mount("/assets", StaticFiles(directory="aset"), name="aset")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

app.add_middleware(SessionMiddleware, secret_key='ifpeoiu83iueoi9028')

@app.get("/health")
async def health():
    return {"status": "ok"}

templates = Jinja2Templates(directory="crm")

@app.post("/web_crm/login")
async def post_login(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    url = "http://localhost:8001/web_default/auth/login"
    dLogin = {"username":form_data.username,"password":form_data.password}
    res = requests.post(url,data=dLogin)
    response = res.json()
    print(response["access_token"])
    request.session['token'] = response["access_token"]
    return res.json()

app.include_router(crm_partner,prefix="/web_crm/partner",tags=["crm partner"],responses={404: {"description": "Not found"}})
app.mount("/web_crm/partner/assets", StaticFiles(directory="aset"))
app.include_router(crm_client,prefix="/web_crm/client",tags=["crm client"],responses={404: {"description": "Not found"}})
app.mount("/web_crm/client/assets", StaticFiles(directory="aset"))
app.include_router(crm_validator,prefix="/web_crm/validator",tags=["crm validator"],responses={404: {"description": "Not found"}})
app.mount("/web_crm/validator/assets", StaticFiles(directory="aset"))
