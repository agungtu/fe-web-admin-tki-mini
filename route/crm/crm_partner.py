# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import APIRouter, Request, Depends,Form
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse,RedirectResponse
import requests
import starlette.status as status

# from requests.api import request
# from starlette.responses import RedirectResponse

crm_partner = APIRouter()

templates = Jinja2Templates(directory="./crm")

@crm_partner.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    # print(token)
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})
    

@crm_partner.get("/signup", response_class=HTMLResponse)
async def get_signup(request: Request):
    return templates.TemplateResponse("signup.html", {"request": request, "tp":"partner", "tipe":"Partner"})


@crm_partner.get("/home", response_class=HTMLResponse)
async def get_home(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

@crm_partner.get("/leads", response_class=HTMLResponse)
async def get_leads(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"leads"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

@crm_partner.get("/testimony-detail", response_class=HTMLResponse)
async def get_lead_detail(request: Request):
    token = request.session.get('token')
   

    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"testimony_detail"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

  # TESTIMONY SHOW DATA
@crm_partner.get("/testimony", response_class=HTMLResponse)
async def get_testimony(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/get_testimony/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"testimony", "data": response["content"]})

@crm_partner.get("/testimony-detail", response_class=HTMLResponse)
async def get_client_detail(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"testimony-detail"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

#show data about
@crm_partner.get("/about", response_class=HTMLResponse)
async def get_about(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_about/get_about/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"about", "data": response["content"]})

#add data about
@crm_partner.post("/about", response_class=HTMLResponse)
async def add_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_about/about"
    dataa = {"title":title,"description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/about", status_code=status.HTTP_302_FOUND)

#edit about
@crm_partner.post("/aboutt", response_class=HTMLResponse)
async def edit_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_about/about/" + _id
    dataa = {"title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/about", status_code=status.HTTP_302_FOUND) 


# @crm_partner.get("/about", response_class=HTMLResponse)
# async def get_client(request: Request):
#     token = request.session.get('token')
#     if token is not None:
#         return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"client"})
#     else:
#         return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

@crm_partner.get("/about-detail", response_class=HTMLResponse)
async def get_client_detail(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"client_detail"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

@crm_partner.get("/report", response_class=HTMLResponse)
async def get_report(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"report"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

@crm_partner.get("/logout", response_class=HTMLResponse)
async def get_logout(request: Request):
    request.session['token'] = None
    return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})


# ADD TESTIMONY

@crm_partner.post("/testimony", response_class=HTMLResponse)
async def add_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony"
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/testimony", status_code=status.HTTP_302_FOUND)


 #edit testimonny
@crm_partner.post("/testimonyy", response_class=HTMLResponse)
async def edit_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony/" + _id
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/testimony", status_code=status.HTTP_302_FOUND) 

#Delete testimony 
@crm_partner.get("/testimony/delete/{id_}")
async def delete_testimony(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_testimony/testimony/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/testimony", status_code=status.HTTP_302_FOUND) 

#delete about
@crm_partner.get("/about/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_about/about/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/about", status_code=status.HTTP_302_FOUND) 

#show data faq
@crm_partner.get("/faq", response_class=HTMLResponse)
async def get_faq(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_faq/get_faq"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"faq", "data": response["content"]})

# ADD FAQ
@crm_partner.post("/faq", response_class=HTMLResponse)
async def add_testimony(
        request: Request, 
        question: str = Form(...),
        answer: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq"
    dataa = {"question":question, "answer":answer, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/faq", status_code=status.HTTP_302_FOUND)

    #edit faq
@crm_partner.post("/faqq", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        question: str = Form(...),
        answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq/" + _id
    dataa = {"question": question, "answer": answer, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/faq", status_code=status.HTTP_302_FOUND) 

#delete faq
@crm_partner.get("/faq/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_faq/faq/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/faq", status_code=status.HTTP_302_FOUND)


    #show data News
@crm_partner.get("/news", response_class=HTMLResponse)
async def get_news(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_news/get_news/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"news", "data": response["content"]})

# ADD news
@crm_partner.post("/news", response_class=HTMLResponse)
async def add_news(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_news/news"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/news", status_code=status.HTTP_302_FOUND)


 #edit News
@crm_partner.post("/newsyuk", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_news/news/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/news", status_code=status.HTTP_302_FOUND) 

#delete faq
@crm_partner.get("/news/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_news/news/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/news", status_code=status.HTTP_302_FOUND)

#SHOW ACHIEVEMENT
@crm_partner.get("/achievement", response_class=HTMLResponse)
async def get_achievement(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/get_achievement/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"achievement", "data": response["content"]})

# ADD ACHIEVEMENT
@crm_partner.post("/achievement", response_class=HTMLResponse)
async def add_achievement(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/achievement", status_code=status.HTTP_302_FOUND)

    #edit ACHIEVEMENT
@crm_partner.post("/achievementnih", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/achievement", status_code=status.HTTP_302_FOUND) 

    #delete ACHIEVEMENNT
@crm_partner.get("/achievementnihh/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_achievement/achievement/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/achievement", status_code=status.HTTP_302_FOUND)

#SHOW FEATURE
@crm_partner.get("/feature", response_class=HTMLResponse)
async def get_feature(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_feature/get_feature/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"feature", "data": response["content"]})

# ADD FEATURE
@crm_partner.post("/feature", response_class=HTMLResponse)
async def add_feature(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/feature", status_code=status.HTTP_302_FOUND)

 #edit FEATURE
@crm_partner.post("/featuree", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/feature", status_code=status.HTTP_302_FOUND) 
  
  
#delete FEATURE
@crm_partner.get("/feature/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_feature/feature/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/feature", status_code=status.HTTP_302_FOUND)

    #SHOW PRICING
@crm_partner.get("/pricing", response_class=HTMLResponse)
async def get_pricing(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/get_pricing/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"pricing", "data": response["content"]})

  
# ADD pricing
@crm_partner.post("/pricing", response_class=HTMLResponse)
async def add_pricing(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/pricing", status_code=status.HTTP_302_FOUND)

#edit FEATURE
@crm_partner.post("/pricingg", response_class=HTMLResponse)
async def edit_pricing(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/pricing", status_code=status.HTTP_302_FOUND) 
 
#delete PRICING
@crm_partner.get("/pricing/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_pricing/pricing/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/pricing", status_code=status.HTTP_302_FOUND)

#SHOW PARTNER
@crm_partner.get("/partner", response_class=HTMLResponse)
async def get_partner(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_partner/get_partner/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"partner", "data": response["content"]})

# ADD PARTNER
@crm_partner.post("/partner", response_class=HTMLResponse)
async def add_partner(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/partner", status_code=status.HTTP_302_FOUND)

#edit PARTNER
@crm_partner.post("/partnerr", response_class=HTMLResponse)
async def edit_partner(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/partner", status_code=status.HTTP_302_FOUND) 

#delete PARTNER
@crm_partner.get("/partnerrr/delete/{id_}")
async def delete_partner(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_partner/partner/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/partner", status_code=status.HTTP_302_FOUND)


#SHOW GALLERY
@crm_partner.get("/gallery", response_class=HTMLResponse)
async def get_gallery(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/get_gallery/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"gallery", "data": response["content"]})

# ADD GALLERY
@crm_partner.post("/gallery", response_class=HTMLResponse)
async def add_gallery(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/gallery", status_code=status.HTTP_302_FOUND)

    #edit GALLERY
@crm_partner.post("/galleryy", response_class=HTMLResponse)
async def edit_gallery(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/gallery", status_code=status.HTTP_302_FOUND) 

#delete GALLERY
@crm_partner.get("/galleryui/delete/{id_}")
async def delete_gallery(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_gallery/gallery/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/gallery", status_code=status.HTTP_302_FOUND)

#SHOW VP
@crm_partner.get("/videoproduct", response_class=HTMLResponse)
async def get_videoproduct(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/get_video_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"videoproduct", "data": response["content"]})

# ADD VP
@crm_partner.post("/videoproduct", response_class=HTMLResponse)
async def add_videoproduct(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/videoproduct", status_code=status.HTTP_302_FOUND)

 #edit VP
@crm_partner.post("/videoproductt", response_class=HTMLResponse)
async def edit_videoproductt(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/videoproduct", status_code=status.HTTP_302_FOUND)

#delete VP
@crm_partner.get("/videoproducttt/delete/{id_}")
async def delete_videoproducttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_video_product/video_product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/videoproduct", status_code=status.HTTP_302_FOUND)

#SHOW wekkly
@crm_partner.get("/weekly", response_class=HTMLResponse)
async def get_weekly(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/get_weekly/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"weekly", "data": response["content"]})

# ADD weekly
@crm_partner.post("/weekly", response_class=HTMLResponse)
async def add_weekly(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly"
    dataa = {"title":title, "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/weekly", status_code=status.HTTP_302_FOUND)

#edit WEEKLY
@crm_partner.post("/weeklyy", response_class=HTMLResponse)
async def edit_weeklyy(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/weekly", status_code=status.HTTP_302_FOUND)

#delete VP
@crm_partner.get("/weeklyyy/delete/{id_}")
async def delete_weeklyyy(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_weekly/weekly/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/weekly", status_code=status.HTTP_302_FOUND)

    #SHOW PRODUCT
@crm_partner.get("/product", response_class=HTMLResponse)
async def get_product(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/get_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"product", "data": response["content"]})

# ADD PRODUCT
@crm_partner.post("/product", response_class=HTMLResponse)
async def add_product(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/"
    dataa = {"title":title, "tipe": tipe, "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/product", status_code=status.HTTP_302_FOUND)

#edit PRODUCT
@crm_partner.post("/productt", response_class=HTMLResponse)
async def edit_productt(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/" + _id
    dataa = {"title": title, "tipe": tipe, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/product", status_code=status.HTTP_302_FOUND)

#delete product
@crm_partner.get("/producttt/delete/{id_}")
async def delete_producttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_product/product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/product", status_code=status.HTTP_302_FOUND)

#SHOW SLIDER
@crm_partner.get("/slider", response_class=HTMLResponse)
async def get_slider(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/get_slider/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"slider", "data": response["content"]})

# ADD SLIDER
@crm_partner.post("/slidera", response_class=HTMLResponse)
async def add_slider(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider"
    dataa = {"title":title,  "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/slider", status_code=status.HTTP_302_FOUND)

#edit SLIDER
@crm_partner.post("/sliderr", response_class=HTMLResponse)
async def edit_sliderr(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/slider", status_code=status.HTTP_302_FOUND)

#delete slider
@crm_partner.get("/sliderrr/delete/{id_}")
async def delete_sliderrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://127.0.0.1:8001/web_api/web_slider/slider/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_crm/partner/slider", status_code=status.HTTP_302_FOUND)

# @crm_partner.get("/leads", response_class=HTMLResponse)
# async def get_leads(request: Request):
#     token = request.session.get('token')
#     if token is not None:
#         return templates.TemplateResponse("partner/index.html", {"request": request, "halaman":"leads"})
#     else:
#         return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})