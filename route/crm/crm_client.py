# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import APIRouter, Request, Depends
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
import requests
from starlette.responses import RedirectResponse

crm_client = APIRouter()

templates = Jinja2Templates(directory="./crm")

@crm_client.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("client/index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})
    

@crm_client.get("/signup", response_class=HTMLResponse)
async def get_signup(request: Request):
    return templates.TemplateResponse("signup.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})

@crm_client.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("client/index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})

@crm_client.get("/home", response_class=HTMLResponse)
async def get_home(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("client/index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})

@crm_client.get("/user", response_class=HTMLResponse)
async def get_leads(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("client/index.html", {"request": request, "halaman":"user"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})

@crm_client.get("/logout", response_class=HTMLResponse)
async def get_logout(request: Request):
    request.session['token'] = None
    return templates.TemplateResponse("login.html", {"request": request, "tp":"client", "tipe":"Client Institusi"})