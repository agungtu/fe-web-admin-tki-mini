# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import APIRouter, Request, Depends
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
import requests
from starlette.responses import RedirectResponse

crm_validator = APIRouter()

templates = Jinja2Templates(directory="./crm")

@crm_validator.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("validator/index.html", {"request": request})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"validator", "tipe":"Validator"})


@crm_validator.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("validator/index.html", {"request": request})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"validator", "tipe":"Validator"})

@crm_validator.get("/home", response_class=HTMLResponse)
async def get_home(request: Request):
    return await get_login(request)

@crm_validator.get("/logout", response_class=HTMLResponse)
async def get_logout(request: Request):
    request.session['token'] = None
    return templates.TemplateResponse("login.html", {"request": request, "tp":"validator", "tipe":"Validator"})